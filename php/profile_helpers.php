<?php

function display_project($project)
{
    echo '<li class="list-group-item col-md-6">';

    $start = date_format(date_create($project['startDate']), 'F Y');
    $end = date_format(date_create($project['endDate']), 'F Y');

    echo "<h3>$project[name] <span style='float:right;'>$start - $end</span></h3>";
    echo '<p class="card-text">';
    echo '<h4>Description</h4>';
    echo "$project[description]<br>";

    if(isset($project['website'])) {
        echo '<h4>Website</h4>';
        echo "<a href='$project[website]'>$project[website]</a><br>";
    }

    if(isset($project['source'])) {
        echo '<h4>Source Code</h4>';
        echo "<a href='$project[source]'>$project[source]</a><br>";
    }

    if(isset($project['videos'])) {
        echo '<h4>Videos</h4>';
        echo "<iframe src='$project[videos]' frameborder='0' allowfullscreen></iframe>";
    }
    echo '<br>';
    echo '</p>';

    echo '</li">';
}

function display_education($school)
{
    $start = date_format(date_create($school['startDate']), 'F Y');
    $end = date_format(date_create($school['endDate']), 'F Y');

    echo '<div class="row">';
    echo '<div class="col-md-4">';
    echo "<h4>$school[name]</h4>";
    echo "<p>$start -  $end</p>";
    echo '</div>';
    echo '<div class="col-md-8">';
    echo "<strong>$school[degree]</strong>";
    echo "<p>$school[description]</p>";
    echo '</div>';
    echo '</div>';
}

function display_skill($skill)
{
    echo "<li class='list-group-item ";
    switch($skill['proficiency']) {
        case 'lowProf':
            echo "list-group-item-danger";
            break;
        case 'knowBasics':
            echo "list-group-item-warning";
            break;
        case 'belowAverageProf':
            echo "list-group-item-warning";
            break;
        case 'averageProf':
            echo "list-group-item-warning";
            break;
        case 'aboveAverageProf':
            echo "list-group-item-success";
            break;
        case 'expertProf':
            echo "list-group-item-success";
            break;
    }
    echo  "'> $skill[name]</li>";
}

function surround_repetition($beginning, $inner_list, $ending,  $formatter, $per_line=NULL)
{
    if(is_int($per_line)) {
        $groups = array_chunk($inner_list, $per_line);
        foreach($groups as $index => $chunk) {
            echo $beginning;

            foreach($chunk as $key => $value) {
                echo $formatter($value);
            }

            echo $ending;
        }
    } else {
        foreach($inner_list as $index => $chunk) {
            echo $beginning;
            echo $formatter($chunk);
            echo $ending;
        }
    }


}