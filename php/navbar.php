<?php

function generate_links($links, $current_page, $is_entry)
{
    assert(is_array($links), true);
    $result = "";
    foreach ($links as $key => $item) {
        $result .= "<li";
        if ($current_page == $key) {
            $result .= ' class="active"';
        }
        $result .= ">";

        if ($is_entry == $item['is_entry']) {
            $result .= "<a onclick='$item[action]' href='$item[link]'>";
            if ($item['icon']) $result .= "<span class='glyphicon $item[icon]'></span>";
            $result .= $item['name'];
            $result .= "</a>";
        }

        $result .= "</li>";
    }

    return $result;
}

function get_navbar_items($current_page)
{
    $entry_pages = array("index", "register", "login");
    $main_pages = array("profile", "messages", "settings", "search");
    $is_entry = in_array($current_page, $entry_pages);
    $left_links = array(
        "profile"  => ["name" => "Profile",  "icon" => "", "link" => "profile.php",  "action" => "", "is_entry" => false],
        "messages" => ["name" => "Messages", "icon" => "", "link" => "messages.php", "action" => "", "is_entry" => false],
        "settings" => ["name" => "Settings", "icon" => "", "link" => "settings.php", "action" => "", "is_entry" => false]
    );

    $right_links = array(
        // "search"   => ["name" => " Search",  "icon" => "glyphicon-search",  "link" => "search.php",   "action" => "",           "is_entry" => false],
        "register" => ["name" => " Sign Up", "icon" => "glyphicon-user",    "link" => "register.php", "action" => "",           "is_entry" => true ],
        "login"    => ["name" => " Login",   "icon" => "glyphicon-log-in",  "link" => "login.php",    "action" => "",           "is_entry" => true ],
        "logout"   => ["name" => " Logout",  "icon" => "glyphicon-log-out", "link" => "index.php",    "action" => "signOut();", "is_entry" => false]
    );

    $result  = '<ul class="nav navbar-nav">' . generate_links($left_links, $current_page, $is_entry) . "</ul>";
    $result .= '<ul class="nav navbar-nav navbar-right">' . generate_links($right_links, $current_page, $is_entry) . "</ul>";
    return $result;
}

function get_navbar()
{
    $current_page = basename($_SERVER['PHP_SELF'], ".php");

    echo '<nav class="navbar navbar-inverse navbar-static-top">';
    echo '<div class="container-fluid">';
    echo '<div class="navbar-header">';
    echo '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarResponsive">';
    echo '<span class="icon-bar"></span>';
    echo '<span class="icon-bar"></span>';
    echo '<span class="icon-bar"></span>';
    echo '</button>';
    echo '<a class="navbar-brand" href="index.php">DevHub</a>';
    echo '</div>';
    echo '<div class="collapse navbar-collapse" id="navbarResponsive">';
    echo get_navbar_items($current_page);
    echo '</div>';
    echo '</div>';
    echo '</nav>';
}