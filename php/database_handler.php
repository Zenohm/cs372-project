<?php
function connect_to_db()
{
    $connection = mysqli_connect("cs372-project.c8njnh0icx1d.us-west-2.rds.amazonaws.com", "root", "ispsi1997", "master", "3306");

    if ($connection->connect_error) {
        die('Connect Error (' . $connection->connect_errno . ') '
            . $connection->connect_error);
    }

    return $connection;
}

function select($connection, $query)
{
    // echo "<h1>" . $query . "</h1>";
    $query_result = mysqli_query($connection, $query);

    if ($query_result && mysqli_num_rows($query_result) != 0) {
        $results = mysqli_fetch_all($query_result, MYSQLI_ASSOC);
    } else {
        // Could not find the identifier in the database.
        $results = null;
    }

    return $results;
}

function insert($connection, $query)
{
    // echo "<h1>" . $query . "</h1>";
    $query_result = mysqli_query($connection, $query);

    if ($query_result) {
        $result = mysqli_insert_id($connection);
    } else {
        $result = null;
    }

    return $result;
}

/**
 * The data in this case is the query key-value pairs where the key is the table fieldname
 * and the value is the value that will be placed under that fieldname.
 */
function build_insert($connection, $table, $data)
{
    $keys = "";
    $values = "";
    $keyvalues = "";

    foreach ($data as $key => $value) {
        if (!$value) {
            continue;
        }

        $keys .= "`$key`";
        $keyvalues .= "`$key`=";
        if(is_string($value)) {
            $values .= "'" . mysqli_real_escape_string($connection, $value) . "'";
            $keyvalues .= "'" . mysqli_real_escape_string($connection, $value) . "'";
        } else {
            $values .= mysqli_real_escape_string($connection, $value);
            $keyvalues .= mysqli_real_escape_string($connection, $value);
        }

        if (next($data) !== FALSE) {
            $keys .= ", ";
            $values .= ", ";
            $keyvalues .= ", ";
        }
    }
    $keys = rtrim($keys,", ");
    $values = rtrim($values,", ");
    $keyvalues = rtrim($keyvalues,", ");

    return "INSERT INTO $table ($keys) VALUES ($values) ON DUPLICATE KEY UPDATE $keyvalues;";
}

function update_user_section_data($connection, $table, $section)
{
    $table = mysqli_real_escape_string($connection, $table);

    $result = [];

    if ($section) {
        foreach($section as $index => $row) {
            if (array_key_exists("delete", $row)) {
                $result[$key] = delete_section_row($connection, $table, $row);
            } else {
                $result[$index] = insert($connection, build_insert($connection, $table, $row));
            }
        }
    }

    return $result;
}

/**
 * We assume the first field in the row is the primary key!
 * This should probably be changed in the future so there is key with
 * a predefined name that designates it as the primary key.
 */
function delete_section_row($connection, $table, $row)
{
    $table = mysqli_real_escape_string($connection, $table);

    $id = reset($row);
    $field_name = key($row);
    $id = mysqli_real_escape_string($connection, $id);
    $field_name = mysqli_real_escape_string($connection, $field_name);
    $query = "DELETE FROM $table WHERE $field_name=" . (is_string($id) ? "'$id'" : "$id") . ";";
    $result = insert($connection, $query);
    return $result;
}

/**
 * Given an identifier for a user,
 * find that user's id and password hash.
 *
 * Returns   id && hash: The user was found and their password hash is present.
 *           id && null: Illegal State. This should never happen.
 *         null && hash: Illegal State. This should never happen.
 *         null && null: The user was not found in the database.
 */
function access_user($connection, $identifier, $sql_identifier)
{
    $sql_identifier = mysqli_real_escape_string($connection, $sql_identifier);
    $identifier = mysqli_real_escape_string($connection, $identifier);
    $query = "SELECT userId, password_hash FROM users WHERE $sql_identifier='$identifier';";
    $user = select($connection, $query);

    if($user) {
        $id = $user[0]["userId"];
        $hash = $user[0]["password_hash"];
    } else {
        $id = null;
        $hash = null;
    }

    return ["id" => $id, "hash" => $hash];
}

/**
 * Given the information for a user, add them to the database.
 *
 * Returns the newly created user's id and password hash.
 */
function register_user($connection, $name, $email, $birthday, $gender, $password)
{
    $name = mysqli_real_escape_string($connection, $name);
    $email = mysqli_real_escape_string($connection, $email);
    $birthday = mysqli_real_escape_string($connection, $birthday);
    $gender = mysqli_real_escape_string($connection, $gender);
    $hash = hide_password($password);
    $query = "INSERT INTO users (name, email, birthday, gender, password_hash) VALUES ('$name', '$email', '$birthday', '$gender', '$hash');";
    $id = insert($connection, $query);

    if (!$id) {
        // Unable to insert the user into the database.
        $hash = null;
    }

    return ["id" => $id, "hash" => $hash];
}

/**
 * Given a user's id and password, check that information against what's
 * stored in the database. This is the user's identity check.
 *
 * Returns true only if the user exists and has the correct password.
 * If a password does not exist for the user, then true will be returned.
 */
function verify_password($connection, $id, $password)
{
    $user = access_user($connection, $id, "userId");
    return $user["id"] != null && ($user["hash"] ? password_verify($password, $user["hash"]) : true);
}

/**
 * Given an identifier for a user
 * find all of that user's data.
 *
 * Returns   An associative array where each key represents a category of user
 *           data. These keys are as follows:
 *                  primary,
 *                  projects,
 *                  experience,
 *                  education,
 *                  skills,
 *                  software,
 *                  links
 *          The values are associative arrays with pieces of information about
 *          the user. If a value for an associated key is null, that means
 *          there is no data stored there.
 *
 */
function get_user_data($connection, $identifier, $sql_identifier="userId")
{
    $user = access_user($connection, $identifier, $sql_identifier);
    $result = ["primary" => "SELECT * FROM users WHERE userId='$user[id]';",
               "projects" => "SELECT * FROM projects WHERE userId='$user[id]';",
               "experience" => "SELECT * FROM experience WHERE userId='$user[id]';",
               "education" => "SELECT * FROM education WHERE userId='$user[id]';",
               "skills" => "SELECT * FROM skills WHERE userId='$user[id]';",
               "software" => "SELECT * FROM software WHERE userId='$user[id]';",
               "links" => "SELECT * FROM links WHERE userId='$user[id]';"
              ];

    foreach ($result as $key => $value) {
        $result[$key] = select($connection, $value);
    }

    return $result;
}

function set_user_data($connection, $data)
{
    // $user = access_user($connection, $identifier, $sql_identifier);
    $result = ["primary" => "users",
               "projects" => "projects",
               "experience" => "experience",
               "education" => "education",
               "skills" => "skills",
               "software" => "software",
               "links" => "links"
              ];

    foreach ($result as $key => $value) {
        $result[$key] = update_user_section_data($connection, $value, $data[$key]);
    }

    return $result;
}

/**
 * Securely change the user's password with a timing attack safe check
 * for security.
 *
 * @param old_password Original plaintext password
 * @param new_password New plaintext password
 *
 * Returns   id && hash: This is the user's id and the new hash.
 *           id && null: Illegal State. This should never happen.
 *         null && hash: Illegal State. This should never happen.
 *         null && null: There was some error that stopped the password from being changed.
 */
function change_password($connection, $id, $old_password, $new_password)
{
    if (strcmp($old_password, $new_password) == 0) {
        return ["id" => $id, "hash" => hide_password($old_password)];
    }

    // Ensure user password safety even when they leave their account logged in.
    if (verify_password($connection, $id, $old_password)) {
        $hash = hide_password($new_password);
        $query = "UPDATE users SET password_hash = '$hash' WHERE userId='$id';";
        $query_result = mysqli_query($connection, $query);

        if ($query_result) {
            $id = mysqli_insert_id($connection);
        } else {
            // Unable to insert the user into the database.
            $id = null;
            $hash = null;
        }

        return ["id" => $id, "hash" => $hash];
    } else {
        // Someone's trying to fuck around with the change password functionality.
        // They probably changed their user id or password hash manually
        // after getting on the settings page.

        die("Your attempts to break the change-password functionality have been recorded.");
        // Don't record the attempt.
    }
}

function hide_password($plaintext)
{
    return password_hash($plaintext, PASSWORD_DEFAULT);
}

function output_user_data($user)
{
    foreach ($user as $category => $values) {
        print "<b><i>$category</i></b> {<br>";
        if ($values) {
            foreach ($values as $index => $row) {
                print "&nbsp;<b>$index</b> {<br>";
                foreach ($row as $key => $value) {
                    print "&nbsp;&nbsp;$key: $value<br>";
                }
                print "&nbsp;}<br>";
            }
        }
        print "}<br>";
    }
}
