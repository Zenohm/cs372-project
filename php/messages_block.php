<?php
require_once("database_handler.php");
session_start();

$connection = connect_to_db();
$userID = $_SESSION["id"];
$userPass = $_SESSION["hash"];

if(!isset($_POST["partner"])){
  $_POST["partner"] = 1;
}


// function get_sent_received($connection, $identifier, $otherUser)
// {
//     $query = "SELECT a.name as sender, b.name as receiver, m.message as message, m.time as time FROM messages m INNER JOIN users a ON m.sender = a.userId INNER JOIN users b ON m.receiver = b.userId WHERE (sender='$identifier' OR receiver='$identifier') AND (sender='$otherUser' OR receiver='$otherUser') ORDER BY time DESC;";
//     return select($connection, $query);
// }

function get_sent_received($connection, $identifier, $otherUser)
{
    $query = "SELECT * FROM messages WHERE (sender='$identifier' OR receiver='$identifier') AND (sender='$otherUser' OR receiver='$otherUser') ORDER BY time DESC;";
    return select($connection, $query);
}

//RECEIVED MESSAGES
$received = get_sent_received($connection, $_SESSION["id"], $_POST["partner"]);

// These functions work.
if(isset($received[0]["sender"])){
    foreach($received as $index => $mail) {
        $sender = $mail["sender"];
        $receiver = $mail["receiver"];
        $message = $mail["message"];
        $time = $mail["time"];
        $title = $mail["title"];
        $sending_user = select($connection, "SELECT name FROM users WHERE userId=$sender");
        $to_from_string = "From: " . $sending_user[0]["name"];
            //changes display to show recipient if it was a sent message-- is working
        if($sender == $_SESSION["id"]){
            $receiving_user = select($connection, "SELECT name FROM users WHERE userId=$receiver");
            $to_from_string = "To: " . $receiving_user[0]["name"];
            // $
        }

        print '<div class="msg">';
        print '<div class="media-body">';
        print '<small class="pull-right time"><i class="fa fa-clock-o"></i>' . $time . '</small>';
        print '<h5 class="media-heading">' . $to_from_string . '</h5>';
        print '<small class="col-sm-11">' . $message . '</small>';
        print '</div>';
        print '</div>';
    }
}

?>