<?php
require_once('database_handler.php');
session_start();

if(isset($_POST['logout'])) {
    logout();
} elseif(isset($_POST['login'])) {
    login();
} elseif (isset($_POST['register'])) {
    register();
} elseif (isset($_POST['id_token'])) {
    // TODO Do some verification and stuff here of the token.
    // Not sure how I'll do this for Facebook because fuck Facebook.

    $use_name = !isset($_POST['email']);

    $connection = connect_to_db();

    if (!empty($_POST['email'])) {
        $user = access_user($connection, $_POST["email"], "email");
    } else {
        $user = access_user($connection, $_POST["name"], "name");
    }

    if ($user["id"]) {
        $new_user = get_user_data($connection, $user['id']);
        if($new_user['primary'][0]['picture'] === null) {
            $new_user['primary'][0]['picture'] = $_POST['image'];
            set_user_data($connection, $new_user);
        }
        $_SESSION["id"] = $user["id"];
        $_SESSION["hash"] = $user["hash"];
        header('Location: ../profile.php');
    } else {
        $user = register_user($connection, $_POST["name"], $_POST["email"], "2000-01-01", "Male", "");
        if(!$user["id"]) {
            die("Unable to register the user in the database (mysqli_query returned false in register_user in database_handler).");
        } else {
            $new_user = get_user_data($connection, $user['id']);
            $new_user['primary'][0]['picture'] = $_POST['image'];
            set_user_data($connection, $new_user);

            $_SESSION["id"] = $user["id"];
            $_SESSION["hash"] = $user["hash"];
            header('Location: ../profile.php');
        }
    }
}

function login()
{
    // if form was actually submitted, check for error
    if (empty($_POST["email"]) || empty($_POST["password"])) {
        $error = true;
        header('Location: ../login.php?error');
    } else {
        $connection = connect_to_db();
        $user = access_user($connection, $_POST["email"], "email");
        $valid_login = false;

        /**/ if (!$user["id"] || !$user["hash"]) { $user_not_found = true; }
        else if (!password_verify($_POST["password"], $user["hash"])) { $incorrect_password = true; }
        else { $valid_login = true; }

        if ($valid_login) {
            $_SESSION["id"] = $user["id"];
            $_SESSION["hash"] = $user["hash"];
            header('Location: ../profile.php');
        } else {
            if(isset($user_not_found)) header('Location: ../login.php?user_not_found');
            if(isset($incorrect_password)) header('Location: ../login.php?incorrect_password');
        }
    }

}

function logout()
{
    $_SESSION = array();
    session_destroy();
    header('Location: ../index.php');
}

function register()
{
    // if form was actually submitted, check for error
    if (empty($_POST["name"])     || empty($_POST["email"])
    ||  empty($_POST["gender"])   || empty($_POST["birthday"])
    ||  empty($_POST["password"]) || empty($_POST["repeated_password"]))
    {
        $error = true;
        header('Location: ../register.php?error');
    } else {
        $connection = connect_to_db();
        $user = access_user($connection, $_POST["email"], "email");

        // Check to make sure the user doesn't already exist.
        if ($user["id"]) {
            $already_exists = true;
            header('Location: ../register.php?already_exists');
        } else {
            $user = register_user($connection, $_POST["name"], $_POST["email"], $_POST["birthday"], $_POST["gender"], $_POST["password"]);

            if(!$user["id"]) {
                die("Unable to register the user in the database (mysqli_query returned false in register_user in database_handler).");
            } else {
                $_SESSION["id"] = $user["id"];
                $_SESSION["hash"] = $user["hash"];
                header('Location: ../profile.php');
            }
        }
    }
}
