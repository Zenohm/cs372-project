function startAllApps() {
    startFacebookApp();
    startGoogleApp();
}

function signOut() {
    $.post("php/session_backend.php", { logout: true })
        .done(function(data) {
            window.location.replace("index.php");
        });
    signGoogleOut();
    signFacebookOut();
}

function query_session_backend(user_name, user_email, id_token, profile_image) {
    $.post("php/session_backend.php", { name: user_name.trim(), email: user_email.trim(), image: profile_image.trim(), id_token: id_token.trim() })
        .done(function(data) {
            window.location.replace("profile.php");
        });
    // window.location.replace("/project/profile.php");
}


var auth2;
var googleUser = {};

function startGoogleApp() {
    gapi.load('auth2', function() {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '684460940408-ectqpo4j2aoek33se6e452vu57jcrn3j.apps.googleusercontent.com',
            scope: "profile email https://www.googleapis.com/auth/plus.login "
        });
        var element = document.getElementById("customBtn");
        console.log(element.id);
        auth2.attachClickHandler(element, {},
            function(googleUser) {
                // Useful data for your client-side scripts:
                var profile = googleUser.getBasicProfile();
                // console.log("ID: " + profile.getId()); // Don't send this directly to your server!
                // console.log('Full Name: ' + profile.getName());
                sessionStorage.setItem('Full_Name', profile.getName());
                // console.log('Given Name: ' + profile.getGivenName());
                sessionStorage.setItem('Given_Name', profile.getGivenName());
                // console.log('Family Name: ' + profile.getFamilyName());
                sessionStorage.setItem('Family_Name', profile.getFamilyName());
                // console.log("Image URL: " + profile.getImageUrl());
                sessionStorage.setItem('Image_Url', profile.getImageUrl());
                // console.log("Email: " + profile.getEmail());
                sessionStorage.setItem('Email', profile.getEmail());

                // The ID token you need to pass to your backend:
                var id_token = googleUser.getAuthResponse().id_token;
                // console.log("ID Token: " + id_token);
                sessionStorage.setItem('ID_Token', id_token);
                query_session_backend(profile.getName(), profile.getEmail(), id_token, profile.getImageUrl());

                // window.location.replace("/project/profile.php");
            },
            function(error) {
                alert(JSON.stringify(error, undefined, 2));
            }
        );
    });
}

function checkGoogleLogin() {

}

function signGoogleOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function() {
        console.log('User signed out.');
    });
}

function startFacebookApp() {
    window.fbAsyncInit = function() {
        FB.init({
            appId: '734821296716499',
            cookie: true,
            xfbml: true,
            version: 'v2.8'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) { return; }
        js = d.createElement(s);
        js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
}

function checkFacebookLogin() {
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            console.log('Already logged in.');
        }
        else {
            facebookLogin();
        }
    });
}

function facebookLogin() {
    FB.login(function(response) {
        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', function(response) {
                if (response && !response.error) {
                    console.log('Good to see you, ' + response.name + '.');

                    sessionStorage.setItem('Full_Name', response.name);
                    // console.log('Given Name: ' + profile.getGivenName());
                    // sessionStorage.setItem('Given_Name', profile.getGivenName());
                    // console.log('Family Name: ' + profile.getFamilyName());
                    // sessionStorage.setItem('Family_Name', profile.getFamilyName());
                    // console.log("Email: " + profile.getEmail());
                    // sessionStorage.setItem('Email', profile.getEmail());

                    // The ID token you need to pass to your backend:
                    // var id_token = googleUser.getAuthResponse().id_token;
                    // console.log("ID Token: " + id_token);
                    sessionStorage.setItem('ID_Token', response.accessToken);
                }
                else {
                    console.log("Error, there was a response error:" + response.error);
                    alert("Error, there was a response error:" + response.error);
                }
            });
            FB.api('/me/picture', function(response) {
                if (response && !response.error) {
                    // console.log(Object.keys(response));
                    sessionStorage.setItem('Image_Url', response.data.url);
                }
                else {
                    console.log("Error, there was a response error:" + response.error);
                    alert("Error, there was a response error:" + response.error);
                }
            })
            query_session_backend(sessionStorage.getItem("Full_Name"), '', sessionStorage.getItem("ID_Token"), sessionStorage.getItem("Image_Url"));
            // window.location.replace("/project/profile.php");
        }
        else {
            console.log('User cancelled login or did not fully authorize.');
        }
    }, { scope: 'public_profile' });
}

function signFacebookOut() {
    FB.logout(function(response) {
        if (response && !response.error) {
            console.log("User has signed out successfully.");
        }
        else {
            console.log("Error, there was a response error:" + response.error);
            alert("Error, there was a response error:" + response.error);
        }
    });
}


//checks every second to see if
//

var IDLE_TIMEOUT = 600; //seconds
var _idleSecondsCounter = 0;

document.onclick = function() {
    _idleSecondsCounter = 0;
};

document.onmousemove = function() {
    _idleSecondsCounter = 0;
};

document.onkeypress = function() {
    _idleSecondsCounter = 0;
};

window.setInterval(CheckIdleTime, 1000);

function CheckIdleTime() {
    _idleSecondsCounter++;
    var oPanel = document.getElementById("SecondsUntilExpire");
    if (oPanel)
        oPanel.innerHTML = (IDLE_TIMEOUT - _idleSecondsCounter) + "";
    // if (_idleSecondsCounter >= IDLE_TIMEOUT) {
    //     window.location = "index.php";
    //     alert("Time expired!");
    //     signOut();
    // }
}
