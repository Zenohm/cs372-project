/* global $ */
//JQuery function to dynamically add rows to the Job Experience table in settings.html
$('document').ready(function() {
    $('.add_another_JobTable').click(function() {
        $("#jobExperienceTable").append('<tr><td><input type="hidden" name="jobId[]" class="form-control" id="jobId" placeholder="" value="" readonly  type="hidden"/></td><td><input type="text" name="jobTitle[]" class="form-control" id="jobTitle" placeholder="Job Title"/></td><td><input type="text" name="employerName[]" id="employerName" placeholder="Employer" class="form-control"/></td><td><input type="date" name="dateStartWork[]" class="form-control" id="dateStartWork" placeholder="Dates Employed"/></td><td><input type="date" name="dateEndWork[]" class="form-control" id="dateEndWork" /></td><td><textarea name="shortJobDescription[]" class="form-control" id="shortJobDescription" cols="45" rows="5" placeholder="A short Description of your experience."></textarea></td><td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td></tr>');
    });
})

//JQuery function to dynamically add rows to education table in settings.html
$('document').ready(function() {
    $('.add_another_educationTable').click(function() {
        $("#educationTable").append('<tr><td><input type="hidden" name="schoolId[]" class="form-control" id="schoolId" placeholder="" value="" readonly  type="hidden"/></td><td><input type="text" name="collegeName[]" class="form-control" id="collegeName" placeholder="College Institution"/></td> <td><select class="selectpicker form-control" name="degreeType[]" name="degreeType" id="degreeType"><option value="BachelorOfArts" name="BachelorOfArts" id="BachelorOfArts">Bachelors of Arts</option> <option value="BachelorOfScience" name="BachelorOfScience" id="BachelorOfScience">Bachelors of Science</option> <option value="Masters" name="Masters" id="Masters">Masters</option> <option value="Doctorate" name="Doctorate" id="Doctorate">Doctorate</option> </select></td> <td><input type="date" class="form-control" id="startDateEducation" name="startDateEducation[]"/></td> <td><input type="date" class="form-control" id="endDateEducation" name="endDateEducation[]"/></td>  <td><input type="text" class="form-control" id="degreeSubject" name="degreeSubject[]" placeholder="Subject of Study"/></td> <td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td> </tr>');
    });
})

//JQuery function to dynamically add rows to the Programming Language skills table in settings.html <td><label><input type="checkbox" class="form-control checkbox" id="masteryCheck" name="masteryCheck[]"/> Mastery</label></td>
$('document').ready(function() {
    $('.add_another_skillsTable').click(function() {
        $("#skillsTable").append('<tr><td><input type="hidden" name="skillId[]" class="form-control" id="skillId" placeholder="" value="" readonly  type="hidden"/></td><td><input type="text" name="ProgrammingLanguage[]" class="form-control" id="ProgrammingLanguage" placeholder="Programming Language"/></td><td><select class="selectpicker form-control" name="ProgrammingLanguagePicker[]" id="ProgrammingLanguagePicker"><option value="expertProf" id="expertProf" name="expertProf">Expert</option><option value="aboveAverageProf" id="aboveAverageProf" name="aboveAverageProf">Above Average Proficiency</option><option value="averageProf" id="averageProf" name="averageProf">Average Proficiency</option><option value="belowAverageProf" id="belowAverageProf" name="belowAverageProf">Below Average Proficiency</option><option value="knowBasics" id="knowBasicsProf" name="knowBasics">Know The Basics</option><option value="lowProf" id="lowProf" name="lowProf">Just Started Learning</option> </select></td> <td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td></tr>');
    });
})


//JQuery function to dynamically add rows to the Software skills table in settings.html
$('document').ready(function() {
    $('.add_another_SoftwareProfTable').click(function() {
        $("#SoftwareProfTable").append('<tr><td><input type="text" name="softwareSkillId[]" class="form-control" id="softwareSkillId" placeholder="" value="" readonly  type="hidden"/></td><td><input type="text" name="softwareSkill[]" id="softwareSkill" class="form-control" placeholder="Software"/></td><td><select class="selectpicker form-control" name="SoftwareProfPicker[]" id="SoftwareProfPicker"><option value="expertProfSoft" id="expertProfSoft" name="expertProfSoft">Expert</option><option value="aboveAverageProfSoft" id="aboveAverageProfSoft" name="aboveAverageProfSoft">Above Average Proficiency</option> <option value="averageProfSoft" id="averageProfSoft" name="averageProfSoft">Average Proficiency</option><option value="belowAverageProfSoft" id="belowAverageProfSoft" name="belowAverageProfSoft">Below Average Proficiency</option><option value="knowBasicsSoft" id="knowBasicsProfSoft" name="knowBasicsSoft">Know The Basics</option> <option value="lowProfSoft" id="lowProfSoft" name="lowProfSoft">Just Started Learning</option></select></td><td><label> <input type="checkbox" class="form-control checkbox" id="masteryCheckSoft" name="masteryCheckSoft[]"/> Mastery</label></td><td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td></tr>');
    });
})


//javascript function to delete rows from all tables
window.SomeDeleteRowFunction = function SomeDeleteRowFunction(o) {
    var p = o.parentNode.parentNode;
    p.parentNode.removeChild(p);
}

// JAVASCRIPT AND JQUERY FOR DYNAMIC TABS USED IN PROJECTS SECTION OF SETTINGS
var button = '<button class="close" type="button" title="Remove this page">×</button>';
var tabID = 1;


//function resetTab() {
//    var tabs = $("#tab-list li:not(:first)");
//    var len = 1
//    $(tabs).each(function(k, v) {
//        len++;
//        $(this).find('a').html('Project ' + len + button);
//    })
//    tabID--;
//}

$(document).ready(function() {
    //this function adds tabs dynamically to the projects div
    $('#btn-add-tab').click(function() {
        tabID++;
        $('#tab-list').append($('<li><a href="#tab' + tabID + '" role="tab" data-toggle="tab">New Project <button class="close" type="button" title="Remove this page">×</button></a></li>'));
        $('#tab-content').append($('<div class="tab-pane fade" id="tab' + tabID + '"><label class="col-md-3 control-label">Project Name: </label><div class="col-md-8"><input class="form-control" type="text" name="projName[]" id="projName" value="" placeholder="My big project"></div><label class="col-md-3 control-label">Website: </label> <div class="col-md-8"> <input class="form-control" name="projWebsite[]" id="projWebsite" type="text" placeholder="www.yourproject.ru">  </div> <label class="col-md-3 control-label">Start Date: </label> <div class="col-md-8"> <input class="form-control" name="projStartDate[]" id="projStartDate" type="date"> </div> <label class="col-md-3 control-label">End Date: </label> <div class="col-md-8"> <input class="form-control" name="projEndDate[]" id="projEndDate" type="date"> </div> <label class="col-md-3 control-label">Source: </label> <div class="col-md-8"> <input class="form-control" name="projSource[]" id="projSource" type="text" placeholder="Project Source"> </div> <label class="col-md-3 control-label">Video Link: </label> <div class="col-md-8"> <input class="form-control" name="projVideo[]" id="projVideo" type="text" placeholder="www.youtube.com"></div> <label class="col-md-3 control-label">Project Description: </label> <div class="col-md-8"><textarea class="form-control" cols="45" rows="5" type="text" id="projDescription" name="projDescription[]" placeholder="Project Description"></textarea></div><div class="col-md-8"><input class="form-control" type="hidden"  name="projectId[]" id="projectId" value="" placeholder="My big project" readonly></div></div>'));
    });
    $('#tab-list').on('click', '.close', function() {
        var tabID = $(this).parents('a').attr('href');
        $(this).parents('li').remove();
        $(tabID).remove();

        //display first tab
        var tabFirst = $('#tab-list a:first');
        //resetTab();
        tabFirst.tab('show');
    });

    var list = document.getElementById("tab-list");
});

// CHANGE PROFILE PHOTO PREVIEW AFTER IMAGE UPLOAD
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#profile-img-tag').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(document).ready(function() {
    $("#profile-img").change(function() {
        readURL(this);
    });
});

//For displaying and hiding personal info panel, experience panel, and password panel
$(document).ready(function($) {
    $("#personalInfoButton").click(function() {
        $("#personalInfoPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-up').toggleClass('glyphicon-menu-down');
    });
    $("#jobExperienceButton").click(function() {
        $("#jobExperiencePanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
    $("#educationButton").click(function() {
        $("#educationPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
    $("#skillsButton").click(function() {
        $("#skillsPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
    $("#softwareProfButton").click(function() {
        $("#softwareProfPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
    $("#previousProjectsButton").click(function() {
        $("#previousProjectsPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
    $("#changePasswordButton").click(function() {
        $("#changePasswordPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
    $("#thirdPartyButton").click(function() {
        $("#thirdParyPanel").fadeToggle(500);
        $(this).find('i').toggleClass('glyphicon-menu-down').toggleClass('glyphicon-menu-up');
    });
});
