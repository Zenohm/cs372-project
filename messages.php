<!DOCTYPE html>
<?php
require_once("php/navbar.php");
require_once("php/database_handler.php");
require_once("php/message_backend.php");
session_start();
//global variable for holding id of recipient clicked ( defaults to first in list)
$connection = connect_to_db();
$userID = $_SESSION["id"];
$userPass = $_SESSION["hash"];

//create connection object for use in db query function get_user_data($connection, $identifier, $sql_identifier)
$userData = get_user_data($connection, $userID, "userId");
?>
<html>

<head>
    <?php
        $title = "Messages";
        $description = "Page that allows users to communicate with other users.";
        $style = "messages";
        $script = "messages";
        require_once('php/header.php')
    ?>

    <!-- refreshes the messages every 30 seconds -->
    <script type="text/javascript">
        var auto_refresh = setInterval(
            function () { $('#messages_block').load('php/messages_block.php', {partner: $("#receiver_id").val()});}, 5000 // refresh every 5000 milliseconds
        );
    </script>

</head>

<body>

<?php get_navbar(); ?>

<div class="container fill">
    <div class="row chat-wrap">

        <!-- Contacts & Conversations -->
        <div class="col-sm-3 panel-wrap">

            <!--Left Menu / Conversation List-->
            <div class="col-sm-12 section-wrap">

                <!--Header-->
                <div class="row header-wrap">
                    <div class="chat-header col-sm-12">
                        <h4 id='username' style="text-align:center;">Select a contact</h4>
                    </div>
                </div>

                <!--Contacts-->
                <div class="row content-wrap">

                    <!-- CREATE CONTACT LIST FOR SENDING MESSAGES TO -->
                    <?php $users = select($connection, "SELECT userId, name FROM users;");
                          foreach($users as $index => $user): ?>

                            <div class='contact btn' id="user<?php echo $user['userId']; ?>">
                                <div class="media-body">
                                    <h5 class='media-heading'><?php echo $user["name"]; ?></h5>
                                </div>
                            </div>

                            <script type='text/javascript'>
                                $("#user<?php echo $user['userId'] ?>").click(function() {

                                    //Sets the current highlighted section to white; Sets the receiver_id input to the clicked users id; sets the clicked user button to orange
                                    $("#user" + document.getElementById("receiver_id").getAttribute("value")).css("background", "#fff");
                                    document.getElementById("receiver_id").setAttribute("value", "<?php echo $user['userId'] ?>");
                                    $(this).css("background", "#ffa000");
                                    document.getElementById('recipient_header').setAttribute("value", "<?php echo $user['name']; ?>");

                                    //$.post( "php/messages_block.php", { partner: "document.getElementById('receiver_id').getAttribute('value')");
                                    $('#messages_block').load('php/messages_block.php', {partner: $("#receiver_id").val()});
                                });
                            </script>

                    <?php endforeach; ?>

                    <script type='text/javascript'>
                        $("#user1").css("background", "#ffa000");
                    </script>

                </div>
            </div>
        </div>

        <!-- Messages & Info -->
        <div class="col-sm-9 panel-wrap">

            <!--Main Content / Message List-->
            <div class="col-sm-12 section-wrap" id="Messages">

                <!--Header-->
                <div class="row header-wrap">
                    <div class="chat-header col-sm-12">
                        <h4 id='recipient_header'>Chat history</h4>
                    </div>
                </div>

                <!--Messages-->
                <div class="row content-wrap messages" id="messages_block">
                    <?php include('php/messages_block.php') ?>
                </div>

                <!--Message box & Send Button-->
                <form action="php/message_backend.php" id="form" onsubmit="return true;" method = "post">
                    <div class="row send-wrap">
                        <div class="send-message">
                            <div class="message-text">
                                <textarea class="no-resize-bar form-control" name="text" id="text" rows="2" placeholder="Write a message..."></textarea>
                            </div>

                            <input type="hidden" name="receiver_id" id="receiver_id" value="1"/>

                            <div class="send-button">
                                <a class="btn" onclick="sendMessage()">Send <i class="fa fa-send"></i></a>
                            </div>
                        </div>
                    </div>
                </form>

                <!-- posts form and clears textarea -->
                <script type="text/javascript">
                    function sendMessage(){
                        $.post( 'php/message_backend.php', $( '#form' ).serialize());
                        document.getElementById('text').value = "";
                        $('#messages_block').load('php/messages_block.php', {partner: $("#receiver_id").val()});
                    }
                </script>
            </div>
        </div>
    </div>
</div>

</body>

</html>