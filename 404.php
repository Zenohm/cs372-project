<!DOCTYPE html>
<?php
require_once("php/navbar.php");
require_once('php/database_handler.php');
session_start();
?>
<html>

<head>
    <?php
        $title = "Page not found";
        $description = "The page you were trying to access was not found on this server.";
        $style = "404";
        $script = "404";
        require_once('php/header.php')
    ?>
</head>

<body>

<?php get_navbar(); ?>

<div class="container">
    <div class="jumbotron">
        <h1 class="display-3">404 Page Not Found</h1>
        <p class="lead">We couldn't find what you're looking for on <em>DevHub</em>.</p>
    </div>
</div>
<div class="container">
    <div class="body-content">
        <div class="row">
            <div class="col-md-6">
                <h2>What happened?</h2>
                <p class="lead">A 404 error status implies that the file or page that you're looking for could not be
                    found.</p>
            </div>
            <div class="col-md-6">
                <h2>What can I do?</h2>
                <p class="lead">If you're a site visitor</p>
                <p>Please use your browser's back button and check that you're in the right place. If you need immediate
                    assistance, please send us an email instead at <i>nomnom@dumpster.com</i>.</p>
                <p class="lead">If you're the site owner</p>
                <p>Please check that you're in the right place and get in touch with your website provider if you
                    believe this to be an error.</p>
            </div>
        </div>
    </div>
</div>
</body>

</html>
