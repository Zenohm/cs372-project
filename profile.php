<!DOCTYPE html>
<?php
session_start();
if (!isset($_SESSION["id"])) {
    header('Location: login.php');
}

if(isset($_GET["id"])) {
    $userId = $_GET["id"];
} else {
    $userId = $_SESSION["id"];
}

require("php/navbar.php");
require_once('php/database_handler.php');
require_once('php/profile_helpers.php');
$connection = connect_to_db();


$user = get_user_data($connection, $userId, "userId");
?>

<html>

<head>
    <?php
        $title = "Profile";
        $description = "The profile page for a user.";
        $style = "profile";
        $script = "profile";
        require_once('php/header.php')
    ?>

    <link rel="stylesheet" href="css/bootstrap-social.css" type="text/css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<?php get_navbar(); ?>

<link rel="stylesheet" href="css/profile.css" type="text/css"/>

<div class="container">

    <div id="profile" class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h3>About Me</h3>
                <p><?php echo $user["primary"][0]["about"]; ?></p>
            </div>
            <div class="col-md-4 text-center">
                <img id="user_image" alt="User's profile picture"
                     class="img-circle avatar img-circle" src='<?php echo $user['primary'][0]['picture'] ?>' width=250 height=250/>
            </div>
            <div class="col-md-4">
                <h3>Details</h3>
                <address>
                    <i class="fa fa-address-card fa-fw"></i><big id="user_name"> <?php echo $user["primary"][0]["name"]; ?></big><br>
                    <i class="fa fa-institution fa-fw"></i> <?php echo $user["primary"][0]["occupation"]; ?><br>
                    <i class="fa fa-map-marker fa-fw"></i>
                    <!--Improve this-->
                    <small> <?php echo $user['primary'][0]['location']; ?></small>
                    <br>
                    <!--Improve this-->
                    <i class="fa fa-phone fa-fw"></i> <?php echo $user['primary'][0]['phone']; ?><br>
                    <i class="fa fa-envelope fa-fw"></i> <a id="user_email"><?php echo $user["primary"][0]["email"]; ?></a><br>
                    <!--Improve this-->
                    <i class="fa fa-globe fa-fw"></i><a href="<?php echo $user['links'][0]['website']; ?>"> <?php echo $user["links"][0]["website"]; ?></a><br>
                    <i class="fa fa-calendar fa-fw"></i> <?php
                        $dob = date_create($user['primary'][0]['birthday']);
                        echo date_format($dob, 'F jS, Y');
                    ?>
                </address>

                <div class="table-responsive">
                    <table class="table" id="linktable">

                        <tr>
                            <td><i class="fa fa-google fa-fw"></i><a href="<?php echo $user["links"][0]["google"]; ?>"> <?php echo basename($user["links"][0]["google"]); ?></a></td>
                            <td><i class="fa fa-facebook fa-fw"></i><a href="<?php echo $user["links"][0]["facebook"]; ?>"> <?php echo basename($user["links"][0]["facebook"]); ?></a></td>
                            <td><i class="fa fa-twitter fa-fw"></i><a href="<?php echo $user["links"][0]["twitter"]; ?>"> @<?php echo basename($user["links"][0]["twitter"]); ?></a></td>
                        </tr>
                        <tr>

                            <td><i class="fa fa-linkedin fa-fw"></i><a href="<?php echo $user["links"][0]["linkedin"]; ?>"> /in/<?php echo basename($user["links"][0]["linkedin"]); ?></a>
                            </td>
                            <td><i class="fa fa-github fa-fw"></i><a href="<?php echo $user["links"][0]["github"]; ?>"> <?php echo basename($user["links"][0]["github"]); ?></a></td>
                            <td><i class="fa fa-stack-overflow fa-fw"></i><a href="<?php echo $user["links"][0]["stack-overflow"]; ?>"> <?php echo basename($user["links"][0]["stack-overflow"]); ?></a>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </div>
    </div>

    <hr>

    <!-- 1st Row -->
    <?php if ($user['projects']): ?>
    <div class="row">
        <div class="card" id="projects">
            <div class="card-block">
                <h2 class="card-title">Projects</h2>
            </div>

            <div class="container" id="projects">
                <?php surround_repetition('<ul class="list-group list-group-flush" id="projList">', $user['projects'], '</ul>', display_project, 2); ?>
            </div>
        </div>
    </div>

    <br>
    <hr>
    <br>
    <?php endif; ?>


    <!--2nd Row-->
    <?php if ($user['education']): ?>
    <div class="row">
        <div class="card">
            <div class="card-block">
                <h2 class="card-title">Education</h2>
            </div>

            <div class="container" id="education">
                <?php surround_repetition('<div class="row">', $user['education'], '</div>', display_education); ?>
            </div>
        </div>
    </div>

    <br>
    <hr>
    <br>
    <?php endif; ?>

    <?php if ($user['experience']): ?>
    <div class="row">
        <div class="card">
            <div class="card-block">
                <h2 class="card-title">Experience</h2>
            </div>

            <div class="container" id="experience">
                <?php surround_repetition('<div class="row">', $user['experience'], '</div>', display_education); ?>
            </div>


            <!--<div class="card-deck" id="experience">-->
            <!--    <div class="card">-->
            <!--        <div class="card-block">-->
            <!--            <h3 class="card-title">-->
            <!--                Google Software Engineer <span style="float: right">Sept. 2003 - Present</span>-->
            <!--            </h3>-->
            <!--        </div>-->
            <!--        <ul class="list-group list-group-flush" id="expList1">-->
            <!--            <li class="list-group-item">Google Maps Maintenance</li>-->
            <!--            <li class="list-group-item">Google Assistant Development</li>-->
            <!--        </ul>-->
            <!--    </div>-->

            <!--</div>-->
        </div>
    </div>

    <br>
    <hr>
    <br>
    <?php endif; ?>

    <!--3rd Row -->
    <?php if ($user['skills'] || $user['software']): ?>
    <div class="row">
        <div class="card">
            <div class="card-block">
                <h2 class="card-title">
                    Skills
                </h2>
            </div>

            <div class="card-deck" id="tools">
                <?php if ($user['skills']): ?>
                <div class="card col-md-6">
                    <div class="card-block">
                        <h3 class="card-title">Software</h3>
                    </div>

                    <div class="container" id="skills">
                        <?php surround_repetition('<ul class="list-group list-group-flush" id="langList">', $user['skills'], '</ul>', display_skill); ?>
                    </div>
                    </ul>
                <?php endif; ?>
                </div>
            </div>
        </div>

    </div>
    <?php endif; ?>

</div>

</body>

</html>
