<!DOCTYPE html>
<?php
require_once('php/database_handler.php');
require_once("php/navbar.php");
session_start();

if(isset($_GET['error'])) {
    $error = true;
}
if(isset($_GET['user_not_found'])) {
    $valid_login = false;
    $user_not_found = true;
}
if(isset($_GET['incorrect_password'])) {
    $valid_login = false;
    $incorrect_password = true;
}

?>

<html>

<head>
    <?php
        $title = "Login";
        $description = "Allows you to securely log into your accounts.";
        $style = "login";
        $script = "login";
        require_once('php/header.php')
    ?>

    <link rel="stylesheet" href="css/bootstrap-social.css" type="text/css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="https://apis.google.com/js/api:client.js"></script>
</head>

<body onload="startAllApps();">

<?php get_navbar(); ?>

<?php if (isset($error)): ?>
    <label class="center" style="color: red">You must enter something into every field!</label>
<?php endif;?>
<?php if (isset($valid_login) && !$valid_login): ?>
    <?php /**/if (isset($user_not_found)): ?>
        <label class="center" style="color: red">That user does not exist</label>
    <?php elseif (isset($incorrect_password)): ?>
        <label class="center" style="color: red">The password you entered was incorrect.</label>
    <?php endif ?>
<?php endif ?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login to DevHub</h3>
                </div>
                <div class="panel-body">
                    <form role="form" data-toggle="validator" action="php/session_backend.php" method="post">
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input class="form-control" placeholder="address@server.com" name="email" id="email" type="email"
                                   data-error="Email address is required." required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>
                            <input class="form-control" data-minlength="6" placeholder="Password" name="password" id="password"
                                   type="password" data-error="Password is required." required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <!--           <div class="checkbox">-->
                        <!--               <label>-->
                        <!--	<input name="remember" type="checkbox" value="Remember Me"> Remember Me-->
                        <!--</label>-->
                        <!--           </div>-->
                        <input class="btn btn-lg btn-success btn-block" type="submit" name="login" value="Login">
                    </form>

                    <hr/>
                    <div style="text-align: center;">
                        <h4>OR</h4>
                    </div>
                    <hr/>

                    <a onclick="facebookLogin();" class="btn btn-block btn-social btn-facebook">
                        <span class="fa fa-facebook"></span> Sign in with Facebook
                    </a>
                    <!--<a class="btn btn-block btn-social btn-github">-->
                    <!--    <span class="fa fa-github"></span> Sign in with Github-->
                    <!--</a>-->
                    <a id="customBtn" class="btn btn-block btn-social btn-google customGPlusSignIn">
                        <span class="fa fa-google"></span> Sign in with Google
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div style="text-align:center;" class="col-md-4 col-md-offset-4">
        <a href="register.php?forgot_password">Forgot your password?</a>
    </div>
</div>

</body>

</html>
