<!DOCTYPE html>
<?php
require_once('php/database_handler.php');
require_once("php/navbar.php");
session_start();

if(isset($_GET['error'])) {
    $error = true;
}
if(isset($_GET['already_exists'])) {
    $already_exists = true;
}
if(isset($_GET['forgot_password'])) {
    $forgot_password = true;
}

?>

<html>

<head>
    <?php
        $title = "Register";
        $description = "Allows you to register a new account.";
        $style = "register";
        $script = "register";
        require_once('php/header.php')
    ?>

    <link rel="stylesheet" href="css/bootstrap-social.css" type="text/css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
    <script src="https://apis.google.com/js/api:client.js"></script>
</head>

<body onload="startAllApps();">

<?php get_navbar(); ?>

<?php if (isset($error)): ?>
    <label class="center" style="color: red">You must enter something into every field!</label>
<?php endif ?>

<?php if (isset($forgot_password)): ?>
    <label class="center" style="color: red">Did you say: "Oh boy, time for a new account"?</label>
<?php endif ?>

<?php if (isset($already_exists)): ?>
    <label class="center" style="color: red">This username and password already exist. Please login instead.</label>
<?php endif ?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Register for DevHub</h3>
                </div>
                <div class="panel-body">
                    <form data-toggle="validator" role="form" action="php/session_backend.php" method="post">
                        <div class="form-group" name="group">
                            <label class="control-label" for="name">Name</label>
                            <input class="form-control" placeholder="John Smith" id="name" name="name" type="text"
                                   data-error="Name required." required>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group" name="group">
                            <label class="control-label" for="email">Email</label>
                            <input class="form-control" placeholder="address@server.com" id="email" name="email" type="email"
                                   data-error="Email address required."
                                   pattern='^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
                                   required>
                            <div class="help-block with-errors"></div>

                        </div>
                        <div class="form-group" name="group">
                            <label for="birthday">Date of Birth</label>
                            <input id="birthday" name="birthday" type="date">
                        </div>
                        <div class="form-group" name="group">
                            <label for="gender">Gender</label>
                            <select class="form-control" name="gender">
                                <option>Male</option>
                                <option>Female</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div class="form-group" name="group">
                            <label class="control-label" for="pwd">Password</label>
                            <div class="form-group col">
                                <input class="form-control" placeholder="Password" id="pwd" name="password" type="password" required>
                            </div>
                            <div class="form-group col" name="group">
                                <input class="form-control" placeholder="Confirm" id="pwd2" name="repeated_password" data-match="#pwd"
                                       data-match-error="The two passwords do not match." type="password" required>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <input class="btn btn-lg btn-success btn-block" type="submit" name="register" value="Register">
                    </form>

                    <hr/>
                    <center>
                        <h4>OR</h4>
                    </center>
                    <hr/>

                    <a onclick="facebookLogin();" class="btn btn-block btn-social btn-facebook">
                        <span class="fa fa-facebook"></span> Register with Facebook
                    </a>
                    <!--<a class="btn btn-block btn-social btn-github">-->
                    <!--    <span class="fa fa-github"></span> Register with Github-->
                    <!--</a>-->
                    <a id="customBtn" class="btn btn-block btn-social btn-google customGPlusSignIn">
                        <span class="fa fa-google"></span> Register with Google
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>
