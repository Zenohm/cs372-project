<!DOCTYPE html>
<?php
require_once("php/navbar.php");
require_once("php/database_handler.php");
session_start();

$connection = connect_to_db();
$userID = $_SESSION["id"];

// $imagename=$_FILES["profile-img"]["name"];

// //Get the content of the image and then add slashes to it
// $imagetmp=addslashes (file_get_contents($_FILES['profile-img']['tmp_name']));

// //Insert the image name and image content in image_table
// $insert_image="INSERT INTO image_table VALUES('$imagetmp','$imagename')";

// mysql_query($insert_image);


//create connection object for use in db query function get_user_data($connection, $identifier, "userID")
$userData = get_user_data($connection, $userID, "userId");

//if password change form is submitted run change_password($connection, $id, $old_password, $new_password)
if (!empty($_POST['passwordChangeSubmit'])) {
    $new_user = change_password($connection, $userID, $_POST['currentPassword'], $_POST['newPassword']);
    $_SESSION["id"] = $new_user["id"];
    $_SESSION["hash"] = $new_user["hash"];
}

//if third party link button is submitted update third party links
if(!empty($_POST['thirdPartySubmit'])) {
    $userData['links'][0]['website'] = $_POST['websiteLink'];
    $userData['links'][0]['google'] = $_POST['googleLink'];
    $userData['links'][0]['facebook'] = $_POST['facebookLink'];
    $userData['links'][0]['twitter'] = $_POST['twitterLink'];
    $userData['links'][0]['linkedin'] = $_POST['linkedinLink'];
    $userData['links'][0]['github'] = $_POST['githubLink'];
    $userData['links'][0]['stack-overflow'] = $_POST['stackoverflowLink'];
    $userData['links'][0]['userId'] = $_SESSION["id"];

    set_user_data($connection, $userData);
}

//if editProfileSubmit is submitted update profile information
// output_user_data($userData);
if(!empty($_POST['editProfileSubmit'])){
    $userData['primary'][0]['name'] = $_POST['userName'];
    $userData['primary'][0]['email'] = $_POST['userEmail'];
    $userData['primary'][0]['birthday'] = $_POST['userBirthday'];
    $userData['primary'][0]['phone'] = $_POST['userPhoneNumber'];
    $userData['primary'][0]['location'] = $_POST['userLocation'];
    $userData['primary'][0]['occupation'] = $_POST['userOccupation'];
    $userData['primary'][0]['about'] = $_POST['userAbout'];

    // print "<pre>";
    // print_r($_POST);
    // print "</pre>";

    $experience_translation = [
        'jobId' => 'jobId',
        'name' => 'jobTitle',
        'description' => 'shortJobDescription',
        'startDate' => 'dateStartWork',
        'endDate' => 'dateEndWork',
        'employer' => 'employerName'
    ];

    $education_translation = [
        'schoolId' => 'schoolId',
        'name' => 'collegeName',
        'description' => 'degreeSubject',
        'startDate' => 'dateStartWork',
        'endDate' => 'dateEndWork',
        'degree' => 'degreeType'
    ];

    $skills_translation = [
        'skillId' => 'skillId',
        'name' => 'ProgrammingLanguage',
        'proficiency' => 'ProgrammingLanguagePicker'
        // 'mastery' => 'masteryCheck'
    ];

    $software_translation = [
        'software_id' => 'softwareSkillId',
        'name' => 'softwareSkill',
        'proficiency' => 'SoftwareProfPicker',
        'mastery' => 'masteryCheckSoft'
    ];

    $projects_translation = [
        'projectId' => 'projectId',
        'name' => 'projectName',
        'startDate' => 'projStartDate',
        'endDate' => 'projEndDate',
        'description' => 'projDescription',
        'website' => 'projWebsite',
        'source' => 'projSource',
        'videos' => 'projVideo'
    ];

    $translations = [
        'experience' => $experience_translation,
        'education'  => $education_translation,
        'skills'     => $skills_translation,
        // 'software'   => $software_translation,
        'projects'   => $projects_translation
    ];

    foreach($translations as $table_name => $translation)
    {
        $userData[$table_name] = get_updated_table($userData[$table_name], $translation, reset($translation), $userID);
    }

    $userData = handle_photo_upload($userData);
    set_user_data($connection, $userData);
}

$userData = get_user_data($connection, $userID, "userId");
$primaryData = $userData['primary'];
$linksData = $userData['links'];
$educationData = $userData['education'];
$projectsData = $userData['projects'];
$skillsData = $userData['skills'];
$experienceData = $userData['experience'];
$softwareSkillsData = $userData['software'];

// Reset the POST variable after every request.
$_POST = array();


function handle_photo_upload($userData)
{
    //ATTEMPTING USER PHOTO UPLOAD
    //Check if profile photo was set in input
    if(isset($_FILES["profile-img"]["tmp_name"]) && isset($_FILES["profile-img"]["name"])) {
        //location of img folder
        $location = "img/";
        //set the extension of the file
        $ext = pathinfo($_FILES['profile-img']['name'], PATHINFO_EXTENSION);
        $name = $userData["primary"][0]["userId"];
        $tmp_name = $_FILES["profile-img"]["tmp_name"];
        if(($ext == 'png' || $ext == 'jpg' || $ext == 'gif')
        && (isset($tmp_name) && !empty($tmp_name))) {
                    $full_name = $name.'.'.$ext;
                    if(move_uploaded_file($tmp_name, $location . $full_name))
                        $userData["primary"][0]["picture"] = $location . $full_name;
        }
    }

    return $userData;
}


function get_updated_table($table, $translation, $id, $userId)
{
    // Get a list of all the data that the user gave to us.
    $givenData = get_user_input($id, $translation, $userId);

    // if(!$givenData) {
    //     return $table;
    // }

    if ($table && $_POST[$id] !== null) {
        $original_ids = get_column_values($table, $id);
        // Get a list of all the added and removed jobs by their title.

        $deleted_ids = array_diff($original_ids, $_POST[$id]);
        $deleted = mark_for_deletion($table, $id, $deleted_ids);

        // Put the data marked for deletion in the givenData object
        $givenData = array_merge($givenData, $deleted);
    }

    // We now have a list of all the data for the added jobs
    return $givenData;
}

function get_column_values($twoDArray, $field)
{
    $values = array();
    foreach($twoDArray as $key => $value){
        array_push($values, $value[$field]);
    }
    return $values;
}

function mark_for_deletion($originalData, $idField, $deletedIds)
{
    // Given the titles of the deleted item, find them and remove them.
    // Go through each original item
    $deleted = array();
    foreach($originalData as $index => $item) {
        // Check if that item's name is among the deleted
        if (in_array($item[$idField], $deletedIds)) {
            // Delete the item.
            $item['delete'] = true;
            array_push($deleted, $item);
        }
    }
    return $deleted;
}

function get_user_input($iterationField, $translationTable, $userId)
{
    if ($_POST[$iterationField] === null) {
        // echo "<h1>You have nothing stored under $iterationField </h1>";
        return null;
    }

    $givenData = array();
    foreach($_POST[$iterationField] as $index => $ignored) {
        $data = array();
        $data['userID'] = $userId;
        foreach($translationTable as $databaseName => $inputName) {
            $data[$databaseName] = $_POST[$inputName][$index];
        }
        array_push($givenData, $data);
    }
    return $givenData;
}

?>

<html>

<head>
    <?php
        $title = "Settings";
        $description = "Allows users to adjust their personal settings.";
        $style = "settings";
        $script = "settings";
        require_once('php/header.php')
    ?>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
</head>

<body>

<?php get_navbar(); ?>

<div id="container" style="margin=5%">
    <div class="row">
        <div class="col-md-2" id="tabs"> <!-- -->
            <!-- Navigation Pills Column    -->
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a data-toggle="pill" href="#editProfile">Edit Profile</a></li>
                <li><a data-toggle="pill" href="#thirdParty">3rd Party</a></li>
                <!--<li><a data-toggle="pill" href="#filtering">Filtering</a></li>-->
                <li><a data-toggle="pill" href="#privacy">Privacy</a></li>
            </ul>

        </div>
        <!-- Main Content of Page contained in tab content column -->
        <div class="col-md-10 tab-content"> <!-- -->
            <div id="editProfile" class="tab-pane fade in active">
                <h3>Edit Profile</h3>
                <h4>Personal Information
                    <small>
                        <button type="button" class="form-control btn btn-custom" id="personalInfoButton"><i class="glyphicon glyphicon-menu-up"></i></button>
                    </small>
                </h4>


                <form class="form-horizontal" role="form" method="post" name="editProfileForm" enctype="multipart/form-data">
                    <div id="personalInfoPanel">
                        <div class="col-md-11 form-group">
                            <!-- Image -->
                            <div class="text-center col-md-11">
                                <img src="<?php echo $userData['primary'][0]['picture'] ?>" id="profile-img-tag" class="avatar img-circle"
                                     alt="avatar" width=250 height=250>
                                <h6>Upload a profile photo...</h6>

                                <input type="file" name='profile-img' id="profile-img" class="center-block form-control-file"> <!-- deleted accepts="image/* ??" -->
                            </div>
                        </div>

                        <!-- Name Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name: </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="userName" placeholder="Name" value="<?php echo $primaryData[0]['name']?>">
                            </div>
                        </div>

                        <!-- Occupation Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Occupation: </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="userOccupation" placeholder="Occupation" value="<?php echo $primaryData[0]['occupation'] ?>">
                            </div>
                        </div>

                        <!-- Location Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Location: </label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="userLocation" placeholder="Location" value="<?php echo $primaryData[0]['location'] ?>">
                            </div>
                        </div>

                        <!-- Phone Number Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Phone Number:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="userPhoneNumber" placeholder="1-800-YourNumber" value="<?php echo $primaryData[0]['phone'] ?>">
                            </div>
                        </div>

                        <!-- Email Text Input Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="email" name="userEmail" placeholder="janedoe@gmail.com" value="<?php echo $primaryData[0]['email'] ?>">
                            </div>
                        </div>

                        <!-- Date of Birth Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Date of Birth:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="date" name="userBirthday" value="<?php echo $primaryData[0]['birthday'] ?>">
                            </div>
                        </div>

                        <!-- About Me Form Group -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">About Me: </label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows=4 cols=50 name="userAbout" placeholder="All about me!"><?php echo $primaryData[0]['about']?></textarea>
                            </div>
                        </div>

                    </div>


                    <h3>Education & Experience</h3>
                    <h4>Job Experience
                        <small>
                            <button class="form-control btn btn-custom" type="button" id="jobExperienceButton"><i class="glyphicon glyphicon-menu-down"></i></button>
                        </small>
                    </h4>
                    <div id="jobExperiencePanel">
                        <!-- Job Experience Form Group-->
                        <div class="form-group">
                            <div class="col-lg-11">
                                <table class="table" id="jobExperienceTable">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Job Title</th>
                                        <th>Employer</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Job Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(!(count($experienceData)==0)): ?>
                                        <?php for($i = 0; $i < count($experienceData); $i++): ?>
                                            <tr>
                                                <td><input type="hidden" name="jobId[]" class="form-control" id="jobId" value="<?php echo $experienceData[$i]['jobId']?>" readonly/></td>
                                                <td><input type="text" name="jobTitle[]" class="form-control" id="jobTitle" placeholder="Job Title" value="<?php echo $experienceData[$i]['name'] ?>"/></td>
                                                <td><input type="text" name="employerName[]" id="employerName" placeholder="Employer" class="form-control" value="<?php echo $experienceData[$i]['employer'] ?>"/></td>
                                                <td><input type="date" name="dateStartWork[]" class="form-control" id="dateStartWork" value="<?php echo $experienceData[$i]['startDate'] ?>"/></td>
                                                <td><input type="date" name="dateEndWork[]" class="form-control" id="dateEndWork" value="<?php echo $experienceData[$i]['endDate'] ?>" /></td>
                                                <td><textarea name="shortJobDescription[]" class="form-control" id="shortJobDescription" cols="45" rows="5" placeholder="A short Description of your experience."><?php echo $experienceData[$i]['description'] ?></textarea></td>
                                                <td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td>
                                            </tr>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                                <!--<input type="button" class="button add_another_JobTable btn btn-default" value="Add New"/> -->
                                <button type="button" class="btn btn-success add_another_JobTable">Add New <i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    </br>

                    <!-- Education Form Group -->
                    <h4>Education
                        <small>
                            <button type="button" class="form-control btn btn-custom" id="educationButton"><i class="glyphicon glyphicon-menu-down"></i></button>
                        </small>
                    </h4>
                    <div id="educationPanel">
                        <div class="form-group">
                            <div class="col-md-11 table-responsive">
                                <table class="table" id="educationTable">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>College Name</th>
                                            <th>Degree Type</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if(count($educationData) != 0): ?>
                                        <?php for($i = 0; $i < count($educationData); $i++): ?>
                                            <tr>
                                            <td><input type="hidden" name="schoolId[]" class="form-control" id="schoolId" placeholder="" value="<?php echo $educationData[$i]['schoolId']; ?>" readonly/></td>
                                            <td><input type="text" name="collegeName[]" class="form-control" id="collegeName" value="<?php echo $educationData[$i]['name']; ?>" placeholder="College Institution"/></td>
                                            <td><select class="selectpicker form-control" name="degreeType[]" id="degreeType">
                                                    <option name ="BachelorOfArts" id="BachelorOfArts" value="BachelorOfArts" <?php if($educationData[$i]['degree'] == 'Bachelor Of Arts') echo 'selected'; ?>>Bachelors of Arts</option>
                                                    <option value="BachelorOfScience" id="BachelorOfScience" name="BachelorOfScience" <?php if($educationData[$i]['degree'] == 'Bachelor Of Science') echo 'selected'; ?>>Bachelors of Science</option>
                                                    <option value="Masters" name="Masters" id="Masters" <?php if($educationData[$i]['degree'] == 'Masters') echo 'selected'; ?> >Masters</option>
                                                    <option value="Doctorate" name="Doctorate" id="Doctorate" <?php if($educationData[$i]['degree'] == 'Doctorate') echo 'selected'; ?> >Doctorate</option>
                                            </select></td>
                                            <td><input type="date" class="form-control"  id="startDateEducation" name="startDateEducation[]" value="<?php echo $educationData[$i]['startDate']; ?>"/></td>
                                            <td><input type="date" class="form-control" class="form-control" id="endDate[]" name="endDateEducation[]" value="<?php echo $educationData[$i]['endDate']; ?>"/></td>
                                            <td><input type="text" class="form-control" id="degreeSubject" id="degreeSubject" name="degreeSubject[]" value="<?php echo $educationData[$i]['description']; ?>" placeholder="Subject of Study"/></td>
                                            <td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td>
                                            </tr>
                                        <?php endfor; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                                <button type="button" class="button add_another_educationTable btn btn-success">Add New <i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    </br>

                    <!-- Skiills Table Form Group -->
                    <h4>Technology and Language Skills
                        <small>
                            <button type="button" class="form-control btn btn-custom" id="skillsButton"><i class="glyphicon glyphicon-menu-down"></i></button>
                        </small>
                    </h4>
                    <div id="skillsPanel">
                        <div class="form-group">
                            <div class="col-md-11 table-responsive">
                                <table class="table" id="skillsTable">
                                    <thead>
                                        <th></th>
                                        <th>Technology or Language</th>
                                        <th>Proficiency</th>
                                    </thead>
                                    <tbody>
                                    <?php if(count($skillsData)!=0): ?>
                                            <?php foreach($skillsData as $i => $skill): ?>
                                                <tr>
                                                <td><input type="hidden" name="skillId[]" class="form-control" id="skillId" placeholder="" value="<?php echo $skill['skillId']; ?>" readonly  type="hidden"/></td>
                                                <td><input type="text" name="ProgrammingLanguage[]" class="form-control" id="ProgrammingLanguage" value="<?php echo $skill['name']; ?>" placeholder="Programming Language"/></td>
                                                <td>
                                                <select class="selectpicker form-control" name="ProgrammingLanguagePicker[]" id="ProgrammingLanguagePicker">
                                                    <option value="expertProf" id="expertProf" name="expertProf" <?php if($skill['proficiency']=='expertProf') echo "selected"; ?>>Expert</option>
                                                    <option value="aboveAverageProf" id="aboveAverageProf" name="aboveAverageProf" <?php if($skill['proficiency']=='aboveAverageProf') echo "selected"; ?>>Above Average Proficiency</option>
                                                    <option value="averageProf" id="averageProf" name="averageProf" <?php if($skillsData[$i]['proficiency']=='averageProf') echo "selected"; ?>>Average Proficiency</option>
                                                    <option value="belowAverageProf" id="belowAverageProf" name="belowAverageProf" <?php if($skill['proficiency']=='belowAverageProf') echo "selected"; ?>>Below Average Proficiency</option>
                                                    <option value="knowBasics" id="knowBasicsProf" name="knowBasics" <?php if($skill['proficiency']=='knowBasics') echo "selected"; ?>>Know The Basics</option>
                                                    <option value="lowProf" id="lowProf" name="lowProf" <?php if($skill['proficiency']=='lowProf') echo "selected"; ?>>Just Started Learning</option>
                                                </select>
                                                </td>
                                                <td><input type="button" class="btn-danger form-control" value="Delete Row" onclick="SomeDeleteRowFunction(this)"/></td>
                                            </tr>
                                            <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                                <button type="button" class="button add_another_skillsTable btn btn-success">Add New <i class="glyphicon glyphicon-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    </br>

                    <!-- Project Tabs Form Group-->
                    <h4>Previous Projects
                        <small>
                            <button type="button" class="form-control btn btn-custom" id="previousProjectsButton"><i class="glyphicon glyphicon-menu-down"></i></button></button>
                        </small>
                    </h4>
                    <div id="previousProjectsPanel">
                        <div class="form-group">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-9">
                                        <p>
                                            <button id="btn-add-tab" type="button" class="btn btn-success pull-right">Add New <i class="glyphicon glyphicon-plus"></i></button>
                                        </p>
                                        <!-- Nav tabs -->
                                        <ul id="tab-list" class="nav nav-tabs" role="tablist">
                                            <?php for($i=0; $i < count($projectsData); $i++): ?>
                                                    <?php if($i < 1): ?>
                                                        <li class="active"><a <?php echo 'href="#tab' . $i . '"'?> role="tab" data-toggle="tab"><?php echo ($projectsData[$i]['name']) ? $projectsData[$i]['name'] : "Unnamed Project" ?> <button class="close" type="button" title="Remove this page">×</button></a></li>
                                                    <?php else: ?>
                                                        <li><a <?php echo 'href="#tab' . $i . '"'?> role="tab" data-toggle="tab"><?php echo ($projectsData[$i]['name']) ? $projectsData[$i]['name'] : "Unnamed Project" ?> <button class="close" type="button" title="Remove this page">×</button></a></li>
                                                    <?php endif; ?>
                                            <?php endfor; ?>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div id="tab-content" class="tab-content">
                                            <?php if(!(count($projectsData) == 0)): ?>
                                                <?php foreach($projectsData as $i => $project): ?>
                                                    <?php if($i < 1): ?>
                                                        <div class="tab-pane fade in active" name="tab<?php echo $i ?>" id="tab<?php echo $i ?>">
                                                    <?php else: ?>
                                                       <div class="tab-pane fade" name="tab<?php echo $i ?>" id="tab<?php echo $i ?>">
                                                    <?php endif; ?>
                                                        <label class="col-md-3 control-label">Project Name: </label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" type="text" name="projectName[]" id="projectName" value="<?php echo $project['name'] ?>" placeholder="My big project">
                                                        </div>
                                                        <label class="col-md-3 control-label">Website: </label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" type="text" name="projWebsite[]" id="projWebsite" value="<?php echo $project['website'] ?>" placeholder="www.yourproject.ru">
                                                        </div>
                                                        <label class="col-md-3 control-label">Start Date: </label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" name="projStartDate[]" id="projStartDate" value="<?php echo $project['startDate'] ?>" type="date">
                                                        </div>
                                                        <label class="col-md-3 control-label">End Date: </label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" name="projEndDate[]" id="projEndDate" value="<?php echo $project['endDate'] ?>" type="date">
                                                        </div>
                                                        <label class="col-md-3 control-label">Source: </label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" type="text" name="projSource[]" id="projSource" value="<?php echo $project['source'] ?>" placeholder="Project Source">
                                                        </div>
                                                        <label class="col-md-3 control-label">Video Link: </label>
                                                        <div class="col-md-8">
                                                            <input class="form-control" type="text" name="projectVideo[]" id="projVideo" value="<?php echo $project['videos'] ?>" placeholder="www.youtube.com">
                                                        </div>
                                                        <label class="col-md-3 control-label">Project Description: </label>
                                                        <div class="col-md-8">
                                                            <textarea class="form-control" cols="45" rows="5" type="text" name="projDescription[]" id="projDescription"  value=""placeholder="Project Description"><?php echo $project['description'] ?></textarea>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input class="form-control" type="hidden"  name="projectId[]" id="projectId" value="<?php echo $project['projectId'] ?>" placeholder="My big project" readonly>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </br>


                    <!-- Save Button Form Group -->
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary btn-custom" name="editProfileSubmit" value="Save Changes">
                            <span></span>
                            <!-- <input type="reset" class="btn btn-default" value="Cancel"> Reset button -->
                        </div>
                    </div>
                </form>
            </div>
            <div id="thirdParty" class="tab-pane fade">
                <h3>Third Party</h3>
                <h4>Third Party Links
                    <small>
                        <button type="button" class="form-control btn btn-custom" id="thirdPartyButton"><i class="glyphicon glyphicon-menu-up"></i></button>
                    </small>
                </h4>
                <form class="form-horizontal" role="form" data-toggle="validator" method="post" name="thirdPartyForm">
                    <div id="thirdParyPanel">

                        <!-- Third Party links form group-->
                        <div class="form-group">
                            <div class="col-md-8">
                                <ul class="list-unstyled" id="linkList">
                                    <li>Personal Website <input type="text" name="websiteLink"
                                                                                 class="form-control"
                                                                                 placeholder="Personal Link"
                                                                                 value="<?php echo $linksData[0]['website']?>"/></li>
                                    <li><i class="fa fa-google fa-fw"></i><input type="text" name="googleLink"
                                                                                 class="form-control"
                                                                                 placeholder="Google Link"
                                                                                 value="<?php echo $linksData[0]['google']?>"/></li>
                                    <li><i class="fa fa-facebook fa-fw"></i><input type="text" name="facebookLink"
                                                                                   class="form-control"
                                                                                   placeholder="Facebook Link"
                                                                                   value="<?php echo $linksData[0]['facebook']?>"/></li>
                                    <li><i class="fa fa-twitter fa-fw"></i><input type="text" name="twitterLink"
                                                                                  class="form-control"
                                                                                  placeholder="Twitter Link"
                                                                                  value="<?php echo $linksData[0]['twitter']?>"/></li>
                                    <li><i class="fa fa-linkedin fa-fw"></i><input type="text" name="linkedinLink"
                                                                                   class="form-control"
                                                                                   placeholder="LinkedIn Link"
                                                                                   value="<?php echo $linksData[0]['linkedin']?>"/></li>
                                    <li><i class="fa fa-github fa-fw"></i><input type="text" name="githubLink"
                                                                                 class="form-control"
                                                                                 placeholder="Github Link"
                                                                                 value="<?php echo $linksData[0]['github']?>"/></li>
                                    <li><i class="fa fa-stack-overflow fa-fw"></i><input type="text"
                                                                                         class="form-control" name="stackoverflowLink"
                                                                                         placeholder="StackOverflow Link"
                                                                                         value="<?php echo $linksData[0]['stack-overflow']?>"/>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- Save Button Form Group -->
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary btn-custom" name="thirdPartySubmit" value="Save Changes">
                            <span></span>
                            <!-- <input type="reset" class="btn btn-default" value="Cancel"> -->
                        </div>
                    </div>
                </form>
            </div>

            <div id="privacy" class="tab-pane fade">
                <h3>Privacy</h3>
                <h4>Change Password
                    <small>
                        <button type="button" class="form-control btn btn-custom" id="changePasswordButton"><i class="glyphicon glyphicon-menu-up"></i></button>
                    </small>
                </h4>
                <form class="form-horizontal" role="form" data-toggle="validator" name="passwordChange" method="post" id="passwordChangeForm">
                    <div id="changePasswordPanel">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Current Password:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="password" name="currentPassword" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">New Password:</label>
                            <div class="col-md-8">
                                <input class="form-control" id="newPassword" type="password" name="newPassword" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Confirm password:</label>
                            <div class="col-md-8">
                                <input class="form-control" data-match="#newPassword" name="confirmPassword" type="password" value="">
                            </div>
                        </div>

                    </div>
                    <!-- Save Button Form Group -->
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" name="passwordChangeSubmit" class="btn btn-primary btn-custom" value="Save Changes">
                            <span></span>
                            <input type="reset" class="btn btn-default btn-custom" value="Reset">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>

</html>
