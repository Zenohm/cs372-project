<!DOCTYPE html>
<?php
require("php/navbar.php");
require_once('php/database_handler.php');
session_start();
?>
<html>

<head>
    <?php
        $title = "Search";
        $description = "Allows users to search for other users.";
        $style = "search";
        $script = "search";
        require_once('php/header.php')
    ?>
</head>

<body>

<?php get_navbar(); ?>

<div class="container">
    <form role="search">
        <div class="input-group">

            <div class="input-group-btn">
                <button type="button" class="btn btn-search btn-default dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-cog"></span>
                    <span class="label-icon">Search</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="#?type=user&q=">
                            <span class="glyphicon glyphicon-user"></span>
                            <span class="label-icon">Search By User</span>
                        </a>
                    </li>
                    <li>
                        <a href="#?type=organization&q=">
                            <span class="glyphicon glyphicon-book"></span>
                            <span class="label-icon">Search By Organization</span>
                        </a>
                    </li>
                    <li>
                        <a href="#?type=location&q=">
                            <span class="glyphicon glyphicon-road"></span>
                            <span class="label-icon">Search By Location</span>
                        </a>
                    </li>
                </ul>
            </div>

            <input type="text" class="form-control">

            <div class="input-group-btn">
                <button type="button" class="btn btn-search btn-default"><span
                            class="glyphicon glyphicon-search"></span></button>
            </div>

        </div>
    </form>
</div>
</body>

</html>
