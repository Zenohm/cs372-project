-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Linux (x86_64)
--
-- Host: cs372-project.c8njnh0icx1d.us-west-2.rds.amazonaws.com    Database: master
-- ------------------------------------------------------
-- Server version	5.6.37-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `schoolId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `degree` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`schoolId`),
  KEY `userId` (`userId`),
  CONSTRAINT `education_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
INSERT INTO `education` VALUES (1,4,'2015-01-05','2016-09-15','Stanford','Doctorate','Did stuff and things for shits and giggles'),(2,4,'2016-10-16','2017-10-11','Purdue','BachelorOfArts','Lived on the streets for 4 years'),(3,1,'2015-01-05','2016-09-15','Purdue','Masters','Lived on the streets for 4 years'),(4,9,'2013-11-01','2015-04-08','Stanford','Masters','CS'),(7,9,'2016-11-03','2016-11-03','Stanford','Masters','CS'),(14,5,'2015-09-28','2016-10-30','Stanford','BachelorOfArts','fish'),(20,4,'2015-11-28','2016-10-29','IPFW','Masters','Homerowdfk'),(21,1,'2016-10-16','2017-10-11','Stanford','Doctorate','Computer Science');
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience`
--

DROP TABLE IF EXISTS `experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experience` (
  `jobId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `employer` varchar(255) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`jobId`),
  KEY `userId` (`userId`),
  CONSTRAINT `experience_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience`
--

LOCK TABLES `experience` WRITE;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` VALUES (1,4,'2015-01-05','2016-09-15','Google','Software Engineer','                                             I wrote everything and all I got was this shirt                                             '),(2,4,'2016-10-16','2017-10-11','Illuminati','Software Guru','                                             You too can own your own private grimoire today!                                             '),(3,1,'2015-01-05','2016-09-15','Google','Software Engineer','I wrote everything and all I got was this shirt'),(4,1,'2016-10-16','2017-10-11','Illuminati','Software Guru','You too can own your own private grimoire today!'),(6,1,'2013-10-03','2015-10-02','Google','Programmer','Blah. Blah.'),(7,1,'2016-11-02','2018-11-28','Microsoft','Developer','Did the best shit. Really.'),(10,1,'2018-11-25','2021-02-12','Pied Piper','Brogrammer','Had fun.'),(11,5,'2015-09-28','2016-10-30','whoever','manager','                    this is it                    '),(15,9,'2013-11-01','2015-04-08','Google','Programmer','           Did stuff.           '),(27,9,'2016-11-03','2016-11-03','Microsoft','Programmer',' Stuff '),(29,4,'2015-11-28','2016-10-29','myself','the boss','                                   hello                                   '),(30,1,'2014-10-07','2020-01-07','Hoppity','Hippity','DWKJDHAKJ'),(31,1,'2034-01-02','2021-04-03','XYZ','ABC','ABC123XYZ4321');
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `links` (
  `linkId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `website` varchar(100) DEFAULT NULL,
  `google` varchar(100) DEFAULT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `github` varchar(100) DEFAULT NULL,
  `stack-overflow` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`linkId`),
  KEY `userId` (`userId`),
  CONSTRAINT `links_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `links`
--

LOCK TABLES `links` WRITE;
/*!40000 ALTER TABLE `links` DISABLE KEYS */;
INSERT INTO `links` VALUES (1,4,'helloworld.com','google.com','facebook','twitter','linkedIn','githoob','stacks'),(10,1,'google.com','sentherus','sentherus','sentherus','sentherus','sentherus','sentherus');
/*!40000 ALTER TABLE `links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `messageId` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `message` text,
  `time` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`messageId`),
  KEY `sender` (`sender`),
  KEY `receiver` (`receiver`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`sender`) REFERENCES `users` (`userId`) ON DELETE CASCADE,
  CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`receiver`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,1,2,'First message to Ben','2017-12-03 01:26:14','Alpha'),(2,2,1,'First response','2017-12-03 01:26:38','Beta'),(3,1,2,'After you\'re done with this could you work on search as well?','2017-12-03 02:33:51','Search needs done'),(4,1,4,'This is so you can see that messages are working.','2017-12-03 02:53:11','Example Message'),(5,1,4,'We need to be able to do this again tomorrow.','2017-12-03 02:53:50','Example Message 2'),(6,2,1,'First test message sent from Webpage','2017-12-04 07:32:29','default'),(7,1,2,'Testing','2017-12-04 08:12:20','default'),(8,2,8,'teeest','2017-12-04 09:28:18','default'),(9,2,8,'2nd test','2017-12-04 10:09:14','default'),(10,2,8,'2nd test','2017-12-04 10:09:16','default'),(11,2,8,'2nd test','2017-12-04 10:09:17','default'),(12,2,8,'2nd test','2017-12-04 10:09:17','default'),(13,2,8,'3rd test','2017-12-04 10:15:31','default'),(14,2,8,'4th test','2017-12-04 10:16:45','default'),(15,10,1,'I see ghosts.','2017-12-04 13:46:37','default'),(16,10,1,'I see ghosts.','2017-12-04 13:46:43','default'),(17,10,1,'I see ghosts.','2017-12-04 13:46:43','default'),(18,10,1,'I see ghosts.','2017-12-04 13:46:43','default'),(19,10,2,'I see ghosts.\r\n','2017-12-04 13:46:49','default'),(20,10,2,'I see ghosts.\r\n','2017-12-04 13:46:50','default'),(21,10,2,'I see ghosts.\r\n','2017-12-04 13:46:50','default'),(22,1,4,'','2017-12-04 14:06:22','default'),(23,4,4,'Ayyyyy','2017-12-04 16:39:58','default'),(24,4,4,'Ayyyyy','2017-12-04 16:40:02','default'),(25,2,1,'Isaac, Can you please make the arrow in the settings page the same color as the navbar, thanks!','2017-12-06 04:03:40','default'),(26,2,14,'Test, hello Joseppi!','2017-12-06 04:05:28','default'),(27,4,5,'Its a message from me to me hooray!!','2017-12-06 08:25:24','default'),(28,1,2,'Also, does this work?','2017-12-06 09:11:46','default'),(29,1,2,'Well, guess it does.','2017-12-06 09:12:01','default'),(30,1,14,'You win this day. 10/10','2017-12-06 09:18:34','default');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `projectId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `source` varchar(100) DEFAULT NULL,
  `videos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`projectId`),
  KEY `userId` (`userId`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,4,'Oceans 11','2015-01-03','2015-04-13','Did some stuff for NASA','nasa.gov','github.com/torvalds','https://www.youtube.com/embed/vFr3K2DORc8'),(3,4,'El Heisto','2015-01-03','2015-04-13','Did some stuff for gooberment','whitehouse.com','https://github.com/systemd','https://www.youtube.com/embed/vFr3K2DORc8'),(4,1,'El Heisto','2015-01-03','2015-04-13','Did some stuff for gooberment','whitehouse.com','https://github.com/systemd','https://www.youtube.com/embed/vFr3K2DORc8'),(6,1,'Names','2011-10-30','2013-03-07','We did stuff and then reimplemented bogo sort in machine code.','google.com','github.com',NULL),(8,5,'THEPROJECT','2015-11-28','2014-10-29','this is it','reddit','source',NULL),(10,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,2,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skills`
--

DROP TABLE IF EXISTS `skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `skills` (
  `skillId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `proficiency` varchar(100) DEFAULT NULL,
  `mastery` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`skillId`),
  KEY `skills_ibfk_1` (`userId`),
  CONSTRAINT `skills_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skills`
--

LOCK TABLES `skills` WRITE;
/*!40000 ALTER TABLE `skills` DISABLE KEYS */;
INSERT INTO `skills` VALUES (3,1,'Haskell','belowAverageProf',0),(8,1,'Python','aboveAverageProf',NULL),(14,5,'Java','aboveAverageProf',NULL),(15,14,'Java','expertProf',NULL),(21,4,'Python1','expertProf',NULL),(25,4,'Python3','expertProf',NULL),(26,1,'C Sharp','belowAverageProf',NULL),(30,2,'Python','expertProf',NULL);
/*!40000 ALTER TABLE `skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software`
--

DROP TABLE IF EXISTS `software`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `software` (
  `software_id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `proficiency` int(11) NOT NULL,
  `mastery` tinyint(1) NOT NULL,
  PRIMARY KEY (`software_id`),
  KEY `software_ibfk_1` (`userId`),
  CONSTRAINT `software_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software`
--

LOCK TABLES `software` WRITE;
/*!40000 ALTER TABLE `software` DISABLE KEYS */;
INSERT INTO `software` VALUES (9,4,'C9',0,0);
/*!40000 ALTER TABLE `software` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `birthday` date DEFAULT NULL,
  `picture` blob,
  `phone` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `occupation` varchar(100) DEFAULT NULL,
  `about` varchar(3000) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Isaac Smith','$2y$10$2mn/fFl1xYVdQ3tvserO4ukLmZhByA82wH0a5ZppJS.Ux2dR/S.W.','sentherus@gmail.com','1997-01-02','img/1.gif','(260) 123-4567','Fort Wayne, Indiana','Software Engineer','Oh look at all this code I have to write now.','Male'),(2,'Benjamin Treesh','$2y$10$Fa4zYSgdI7aggSYtQh6CA.k3L.TxNr.gKsrvzKlgTak5q7N.36yzu','cortana.j117@gmail.com','1995-06-15','https://lh5.googleusercontent.com/-2dhX4h964E0/AAAAAAAAAAI/AAAAAAAAFUw/WOO2iu1QqeY/s96-c/photo.jpg','260-494-4221','Fort Wayne, IN','Developer Co-Owner at Devhub, Inc','                  I write message pages!  I am a junior Computer Science at IPFW.  Interested in hardware, software, and emerging technologies!               ','Male'),(4,'Matt Ruiz','$2y$10$mkF.VYFtf8QizM7x63agfeXvRq8V4hzcKRzsr2tWSr4WVOSZ8XlVa','ruizmf01@students.ipfw.edu','1992-06-08','img/4.png','8675309','Fort Wayne, Indiana','Programmer','                   I am the master of the settings page                                                 ','Male'),(5,'Matt Reez','$2y$10$4HZiBaYYnG2XF3idjTljN.lwjePp/RyLrY8W35e/1cYeUYc/aGAje','mfruiz33@gmail.com','1992-06-08',NULL,'8675309','Fort Wayne','Hobo','                         Im a nice guy just trying to get through Web App                       ','Male'),(6,'John Smith','$2y$10$RoKt85f6ZkXwPkhxftp5Zu2Vkx1YBHDWSBSndyzgBiBWS7T8C9M4.','john@smith.com','2007-05-02',NULL,NULL,NULL,NULL,NULL,'Male'),(8,'Beanjamin Fargo','$2y$10$L/hn.5m1d8/YvpSSHrAdIuNxx4ZFHX8lq14m6GxCxZIeWL8TD4DVa','g@f.com','1995-06-15',NULL,NULL,NULL,NULL,NULL,'Male'),(9,'10; SELECT * FROM users; /* Muh SQL','$2y$10$69NMihk/HqwcWDqpn39NKux4nNACX72.EBpEzyMJa3jAilNpCjIk2','sql@injection.com','2002-10-01',NULL,NULL,NULL,NULL,'                          ','Other'),(10,'Olivia Dunham','$2y$10$AjJkrJpC9XF7YR5tySTfieTY8WYSxoVB6StZ.KhcVqzgwPdvyNVcq','dude@dudette.com','2022-09-01',NULL,'123 - 654 - 3567','Boston','Detective','  I do stuff on Fringe.','Female'),(13,'senwlaidkjwalkdj','$2y$10$0QsiCy/T.U.Afp7/EIthAu1h6cWujT05KsBtvsBhX5B7.WS7JuoaS','wldkjwlj@dljkwadlkwjadlkwa.com','2014-10-29',NULL,NULL,NULL,NULL,'  ','Other'),(14,'Joeshph Shell','$2y$10$GBCpbKIgsDGp7/F.gcwFWeiSYPyy4D2UCpLvmnp1yqY5NG2L53oSu','crypticconspiracy@gmail.com','1996-09-15','https://lh6.googleusercontent.com/-WyGHPFfcxyE/AAAAAAAAAAI/AAAAAAAAAC8/asOO1xi2b0w/s96-c/photo.jpg',NULL,NULL,NULL,'I like trains.','Male'),(15,'Isaac Smith','$2y$10$lRnfZe.OoeYEWVJkhjNWeOAr.Ys7nO.ddNc4x96cXBxVKEURBqgCC','duuuude@stuff.com','2015-10-03',NULL,NULL,NULL,NULL,NULL,'Male'),(16,'Micah Smith','$2y$10$E8wXqzYCdCXZvyifZgrVJuHtrzjcGUfYRJSuf78JYVPy0FoMIRmXC','smitml01@students.ipfw.edu','1992-09-03','img/16.gif',NULL,NULL,NULL,'                    ','Male');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-06  4:19:14
