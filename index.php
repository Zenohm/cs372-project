<!DOCTYPE html>
<?php
require("php/navbar.php");
require_once('php/database_handler.php');
session_start();
?>
<head>
    <?php
        $title = "DevHub";
        $description = "Welcome to DevHub!";
        $style = "index";
        $script = "index";
        require_once('php/header.php')
    ?>
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id"
          content="684460940408-ectqpo4j2aoek33se6e452vu57jcrn3j.apps.googleusercontent.com">

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet"
          type="text/css">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
</head>

<body>

<?php get_navbar(); ?>

<header class="intro-header">
    <div class="container">
        <div class="intro-message">
            <h1>DevHub</h1>
            <h4>A Social Network for Developers</h4>
            <!--<hr class="intro-divider">-->
            <ul class="list-inline intro-social-buttons">
                <li class="list-inline-item">
                    <a href="register.php" class="btn btn-secondary btn-lg">
                        <i class="fa fa-facebook fa-fw"></i>
                        <span class="network-name">Facebook</span>
                    </a>
                </li>
                <!--<li class="list-inline-item">-->
                <!--    <a href="register.php" class="btn btn-secondary btn-lg">-->
                <!--        <i class="fa fa-github fa-fw"></i>-->
                <!--        <span class="network-name">Github</span>-->
                <!--    </a>-->
                <!--</li>-->
                <li class="list-inline-item">
                    <a href="register.php" class="btn btn-secondary btn-lg">
                        <i class="fa fa-google fa-fw"></i>
                        <span class="network-name">Google</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>

<section class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 ml-auto">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h3 class="section-heading">Keep In Contact and<br>Meet New People</h3>
                <p class="lead">Stay connected with other developers you've met while also being able to introduce
                    yourself to their network of connections.</p>
            </div>
            <div class="col-lg-5 mr-auto">
                <!--<img class="img-fluid" src="img/ipad.png" alt="">-->
            </div>
        </div>
    </div>
</section>

<section class="content-section-b">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 mr-auto order-lg-2">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h3 class="section-heading">Organize Your Projects</h3>
                <p class="lead">Keep all your projects organized with all material relevant to each project available in
                    one, easy to access location on your profile page.
            </div>
            <div class="col-lg-5 ml-auto order-lg-1">
                <!--<img class="img-fluid" src="img/dog.png" alt="">-->
            </div>
        </div>
    </div>
</section>

<section class="content-section-a">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 ml-auto">
                <hr class="section-heading-spacer">
                <div class="clearfix"></div>
                <h3 class="section-heading">Develop Your Profile</h3>
                <p class="lead">Keep all your social media profiles connected and organize all the information about
                    your experience,
                    both personal and professional, in one centralized location. This profile can be passed between you
                    and other developers
                    as an up-to-date representation of your interests, hobbies, and experiences.
            </div>
            <div class="col-lg-5 mr-auto ">
                <!--<img class="img-fluid" src="img/phones.png" alt="">-->
            </div>
        </div>
    </div>
</section>

<aside class="banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 my-auto">
                <h2>Connect to DevHub:</h2>
            </div>
            <div class="col-lg-6 my-auto">
                <ul class="list-inline banner-social-buttons">
                    <li class="list-inline-item">
                        <a href="register.php" class="btn btn-secondary btn-lg">
                            <i class="fa fa-facebook fa-fw"></i>
                            <span class="network-name">Facebook</span>
                        </a>
                    </li>
                    <!--<li class="list-inline-item">-->
                    <!--    <a href="register.php" class="btn btn-secondary btn-lg">-->
                    <!--        <i class="fa fa-github fa-fw"></i>-->
                    <!--        <span class="network-name">Github</span>-->
                    <!--    </a>-->
                    <!--</li>-->
                    <li class="list-inline-item">
                        <a href="register.php" class="btn btn-secondary btn-lg">
                            <i class="fa fa-google fa-fw"></i>
                            <span class="network-name">Google</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</aside>

</body>

</html>
